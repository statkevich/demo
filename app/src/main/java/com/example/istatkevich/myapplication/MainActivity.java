package com.example.istatkevich.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {

    private static final String LOG_TAG = "test";

    private View rootView;
    private RelativeLayout relativeLayout;
    private View greenView;

    private float density;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootView = findViewById(android.R.id.content);
        rootView.setOnTouchListener(this);

        density = getResources().getDisplayMetrics().density;
        createTempLayout();

        Button button = (Button) findViewById(R.id.button);
        button.getLayoutParams();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View view = findViewById(R.id.text);

            displayTouch(event);
            displayPosition(view);
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        int[] relativeLayoutLocationInWindow = new int[2];
        relativeLayout.getLocationInWindow(relativeLayoutLocationInWindow);

        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "relativeLayout locationInWindow x:%d y:%d",
                relativeLayoutLocationInWindow[0],
                relativeLayoutLocationInWindow[1]
        ));


        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "relativeLayout width:%d height:%d",
                relativeLayout.getWidth(),
                relativeLayout.getHeight()
        ));

        int[] greenViewLocationInWindow = new int[2];
        greenView.getLocationInWindow(greenViewLocationInWindow);

        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "greenView locationInWindow x:%d y:%d",
                greenViewLocationInWindow[0],
                greenViewLocationInWindow[1]
        ));


        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "greenView width:%d height:%d",
                greenView.getWidth(),
                greenView.getHeight()
        ));

        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "greenView x:%f y:%f",
                greenView.getX(),
                greenView.getY()
        ));

        Log.d(LOG_TAG, String.format(
                Locale.getDefault(),
                "greenView top:%d left:%d",
                greenView.getTop(),
                greenView.getLeft()
        ));
    }

    private void displayTouch(MotionEvent event) {
        String message = String.format(
                Locale.getDefault(),
                "Touch x:%d y:%d xRaw:%d yRaw:%d",
                pxToDp(event.getX()),
                pxToDp(event.getY()),
                pxToDp(event.getRawX()),
                pxToDp(event.getRawY())
        );

        Log.d(LOG_TAG, message);
    }

    private void displayPosition(View view) {
        int[] locationOnScreen = new int[2];
        view.getLocationOnScreen(locationOnScreen);

        int[] locationOnWindow = new int[2];
        view.getLocationInWindow(locationOnWindow);

        String message = String.format(
                Locale.getDefault(),
                "locationOnScreen x:%d y:%d locationOnWindow x:%d y:%d density:%f",
                pxToDp(locationOnScreen[0]),
                pxToDp(locationOnScreen[1]),
                pxToDp(locationOnWindow[0]),
                pxToDp(locationOnWindow[1]),
                density
        );

        Log.d(LOG_TAG, message);
    }

    private int pxToDp(float px) {
        return (int) (px / density);
    }

    private void createTempLayout() {
        // Create parent view
        relativeLayout = new RelativeLayout(this);

        // Create child view
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                100,
                100
        );
        greenView = new View(this);
        greenView.setLayoutParams(layoutParams);
        greenView.setBackgroundColor(Color.GREEN);

        // Add child view before final action
        relativeLayout.addView(greenView);

        // Change margins after add to parent view
        ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) greenView.getLayoutParams();
        marginParams.setMargins(50, 50, 50, 50);
        greenView.setLayoutParams(marginParams);

        // Final action, measure and layout process
        relativeLayout.measure(
                View.MeasureSpec.makeMeasureSpec(500, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(500, View.MeasureSpec.EXACTLY)
        );

        int layoutWidth = relativeLayout.getMeasuredWidth();
        int layoutHeight = relativeLayout.getMeasuredHeight();
        relativeLayout.layout(0, 0, layoutWidth, layoutHeight);
    }
}
